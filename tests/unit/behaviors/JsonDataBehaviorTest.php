<?php

namespace ox404fff\utils\test\behaviors;

use ox404fff\utils\behaviors\JsonDataBehavior;
use ox404fff\utils\exceptions\JsonDataBehaviorException;
use yii\base\Component;
use yii\codeception\DbTestCase;
use yii\db\ActiveRecord;
use yii\helpers\Json;

class JsonDataBehaviorTest extends DbTestCase
{
    private $_testAttributes = [
        'test1' => 'data1',
        'test2' => 'data2',
        'test3' => 'data3',
    ];

    public function testAttachBehavior()
    {
        $notActiveRecordInstance = new Component();

        try {
            $notActiveRecordInstance->attachBehavior('json_data', [
                'class'     => JsonDataBehavior::className(),
                'attribute' => 'text'
            ]);

            $isThrowsException = false;
            
        } catch (JsonDataBehaviorException $e) {
            $isThrowsException = true;
        }

        $this->assertTrue($isThrowsException,
            'On try attach behavior to class not instanced of yii\db\ActiveRecord must throws exception'
        );
    }

    public function testGetJsonAttribute()
    {
        $model = $this->_createModel();
        $this->assertNull($model->getJsonAttribute('not_exists_attribute'));

        $this->assertEquals($model->getJsonAttribute('test1'), 'data1');
        $this->assertEquals($model->getJsonAttribute('test2'), 'data2');
        $this->assertEquals($model->getJsonAttribute('test3'), 'data3');
    }

    public function testGetJsonAttributeEmpty()
    {
        $model = $this->_createModel(false);
        $this->assertNull($model->getJsonAttribute('not_exists_attribute'));
    }


    public function testSetJsonAttribute()
    {
        $model = $this->_createModel();
        $model->setJsonAttribute('new_test', 'new_data');
        $model->setJsonAttribute('test1', 'new_data1');

        $this->assertEquals($model->getJsonAttribute('test2'), 'data2');
        $this->assertEquals($model->getJsonAttribute('new_test'), 'new_data');
        $this->assertEquals($model->getJsonAttribute('test1'), 'new_data1');
    }

    public function testSetJsonAttributeEmpty()
    {
        $model = $this->_createModel(false);
        $model->setJsonAttribute('new_test', 'new_data');
        $this->assertEquals($model->getJsonAttribute('new_test'), 'new_data');
    }


    public function testGetJsonAttributes()
    {
        $model = $this->_createModel();
        $this->assertEquals($model->getJsonAttributes(), $this->_testAttributes);
    }


    public function testGetJsonAttributesEmpty()
    {
        $model = $this->_createModel(false);
        $this->assertTrue(is_array($model->getJsonAttributes()));
        $this->assertEmpty($model->getJsonAttributes());
    }


    public function testSetJsonAttributes()
    {
        $model = $this->_createModel();
        $model->setJsonAttributes([
            'new_test1' => 'new_data1',
            'new_test2' => 'new_data2',
        ]);

        $this->assertNotEquals($model->getJsonAttribute('test2'), 'data2');
        $this->assertEquals($model->getJsonAttribute('new_test1'), 'new_data1');
    }


    public function testSetJsonAttributesEmpty()
    {
        $model = $this->_createModel(false);
        $model->setJsonAttributes($this->_testAttributes);

        $this->assertEquals($model->getJsonAttributes(), $this->_testAttributes);
    }


    /**
     * @return ActiveRecord
     */
    private function _createModel($setAttributes = true)
    {

        $model = $this->getMockBuilder(ActiveRecord::className())
            ->setMethods(['attributes'])
            ->getMock();

        $model->expects($this->any())->method('attributes')->will($this->returnValue(['text']));

        /**
         * @var \yii\db\ActiveRecord $model
         */
        if ($setAttributes) {
            $model->setAttribute('text', Json::encode($this->_testAttributes));
        }

        $model->attachBehavior('json_data', [
            'class'     => JsonDataBehavior::className(),
            'attribute' => 'text'
        ]);

        return $model;
    }

}
