<?php

namespace ox404fff\utils\behaviors;

use yii\behaviors\TimestampBehavior as BaseTimestampBehavior;

/**
 * Extended timestamp for set updated on multiple update attributes and set deleted time for safe deleted items
 *
 * Class StaticCacheBehavior
 * @package app\base\behaviors
 */
class TimestampBehavior extends BaseTimestampBehavior
{

    
    
}
